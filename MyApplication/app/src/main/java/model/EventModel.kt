package model

import android.widget.DatePicker
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class EventModel (
    var calendar : String? = "",
    var imageResource : String? = "",
    var note : String? = "",
    var uid : String? = ""
)

/*@Exclude
fun toMap() : Map<String, Any?> {
    return mapOf(
        "calendar" to calendar,
        "imageResource" to imageResource,
        "note" to note,
        "uid" to uid
    )
}*/