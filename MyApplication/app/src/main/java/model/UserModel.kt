package model

data class UserModel (
    var firstName : String? = "",
    var lastName : String? = "",
    var occupation : String? = ""
)