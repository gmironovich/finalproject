import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.EventDay
import com.cmu.myapplication.MyEventDay
import com.cmu.myapplication.NotePreviewActivity
import com.cmu.myapplication.R
import com.google.firebase.FirebaseError
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.note_preview_activity.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var mCalendarView: CalendarView
    private val mEventDays = ArrayList<EventDay>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)



        mCalendarView = view.findViewById(R.id.calendarView)
        mCalendarView?.setOnDayClickListener {
                eventDay -> previewNote(eventDay)
        }



        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADD_NOTE || resultCode == RESULT_OK) {
            val myEventDay: MyEventDay = data!!.getParcelableExtra(RESULT)
            mCalendarView.setDate(myEventDay.calendar)
            mEventDays.add(myEventDay)
            mCalendarView.setEvents(mEventDays)
        }
    }

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
        val RESULT = "result"
        val EVENT = "event"
        var formatedDate : String = ""
        private val ADD_NOTE = 44
    }

    /*private fun addNote() {
        val intent = Intent(this, AddFragment::class.java)
        startActivityForResult(intent, ADD_NOTE)
    }*/

    private fun previewNote(eventDay: EventDay) {
        val intent = Intent(getActivity(), NotePreviewActivity::class.java)

        /*if (eventDay is MyEventDay) {
            intent.putExtra(EVENT, eventDay)
        }*/

        val date = mCalendarView.selectedDate

        val sdf = SimpleDateFormat("dd-MM-yyyy")
        formatedDate = sdf.format(date.time)

        System.out.println(formatedDate)

        getActivity()?.startActivity(intent)
    }

}