package com.cmu.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

class MainActivity : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onSignIn(v : View) {
        val userEmail = findViewById<EditText>(R.id.loginEmail)
        val userPassword = findViewById<EditText>(R.id.loginPassword)

        auth.signInWithEmailAndPassword(userEmail.text.toString(), userPassword.text.toString()).addOnCompleteListener(this) {task ->
            if(task.isSuccessful) {
                //val intent = Intent(this, HomeActivity::class.java)
                val intent = Intent(this, NavigationActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun onSignUp(v : View) {
        val intent = Intent(this, RegistrationActivity::class.java)
        startActivity(intent)
    }
}
