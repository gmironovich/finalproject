package com.cmu.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import model.UserModel

class UserInformationActivity : AppCompatActivity() {

    private lateinit var database : DatabaseReference
    private lateinit var auth : FirebaseAuth

    private lateinit var userModel : UserModel

    override fun onCreate(savedInstanceState: Bundle?) {
        database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mikhgrigplanner.firebaseio.com/")
        auth = FirebaseAuth.getInstance()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_information)
    }

    fun onNext(v : View) {
        val currentUser = auth.currentUser!!

        val firstName = findViewById<EditText>(R.id.userFirstName)
        val lastName = findViewById<EditText>(R.id.userLastName)
        val occupation = findViewById<EditText>(R.id.userOccupation)


        val user = UserModel(firstName.text.toString(), lastName.text.toString(), occupation.text.toString())
        database.child("users").child(currentUser.uid!!).setValue(user)

        val intent = Intent(this, NavigationActivity::class.java)
        startActivity(intent)
    }
}
