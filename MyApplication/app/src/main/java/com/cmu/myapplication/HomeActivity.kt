package com.cmu.myapplication

import android.app.DatePickerDialog
import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*

class HomeActivity : AppCompatActivity() {

    private lateinit var database : DatabaseReference
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mikhgrigplanner.firebaseio.com/").child("users")
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser!!

        database.child(currentUser.uid!!).child("firstName").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(String::class.java)
                userWelcome.text = "Welcome, $user"
            }

            override fun onCancelled(databaseError: DatabaseError) {
                print("Failed to read value.")
            }
        })

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun onSignOut(v : View) {
        FirebaseAuth.getInstance().signOut()

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun onChooseDay(v : View) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(this@HomeActivity, DatePickerDialog.OnDateSetListener
        { view, year, monthOfYear, dayOfMonth ->
            calendarDay.setText("" + dayOfMonth + " - " + (monthOfYear+1) + " - " + year)
        }, year, month, day)

        datePickerDialog.show()
    }
}
