package com.cmu.myapplication

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.applandeo.materialcalendarview.EventDay
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import model.EventModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlinx.android.synthetic.main.note_preview_activity.*

class NotePreviewActivity : AppCompatActivity() {
    private lateinit var database : DatabaseReference
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mikhgrigplanner.firebaseio.com/").child("events")
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser!!

        super.onCreate(savedInstanceState)
        setContentView(R.layout.note_preview_activity)

        val note = findViewById<TextView>(R.id.note)

        var events = HashMap<String, EventModel>()

        val dateFormated = HomeFragment.formatedDate

        setTitle(dateFormated)
        note.text = "No Event has been created!"

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if(p0!!.exists()) {
                    val children = p0.children
                    children.forEach {
                        //println(it.value.toString())
                        val uid = it.child("uid").value.toString()
                        val dateOnFile = it.child("day").value.toString()

                        if(currentUser.uid.equals(uid)) {
                            if(dateOnFile.equals(dateFormated)) {
                                note.text = it.child("note").value.toString()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                throw p0.toException()
            }
        })
    }

    companion object {

        fun getFormattedDate(date: Date): String {
            val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
            return simpleDateFormat.format(date)
        }
    }

    fun onDeleteEvent(v : View) {
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser!!

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if(p0!!.exists()) {
                    val children = p0.children
                    children.forEach {
                        val uniqueID = it.ref.toString()

                        val tokens = uniqueID.split("/")
                        val parentKey = tokens[tokens.size - 1]

                        val dateOnFile = it.child("day").value.toString()
                        val uid = it.child("uid").value.toString()
                        val dateFormated = HomeFragment.formatedDate

                        //System.out.println(parentKey)

                        if(currentUser.uid.equals(uid)) {
                            if(dateOnFile.equals(dateFormated)) {
                                database.child(parentKey).removeValue()
                                val intent = Intent(this@NotePreviewActivity, NavigationActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                throw p0.toException()
            }
        })
    }
}
