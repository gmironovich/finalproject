package com.cmu.myapplication

import android.annotation.SuppressLint
import java.util.Calendar;
import android.os.Parcel
import android.os.Parcelable
import com.applandeo.materialcalendarview.EventDay


internal class MyEventDay : EventDay, Parcelable {
    var note: String? = null
        private set

    var uid : String? = null
        private set

    var day : String? = null
        private set

    constructor(uid : String, day: String, imageResource: Int, note: String) : super(null, imageResource) {
        this.uid = uid
        this.day = day
        this.note = note
    }

    private constructor(`in`: Parcel) : super(`in`.readSerializable() as Calendar, `in`.readInt()) {
        note = `in`.readString()
    }

 override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(uid)
        parcel.writeString(day)
        parcel.writeInt(getImageResource())
        parcel.writeString(note)
    }
    override fun describeContents(): Int {
        return 0
    }



    companion object CREATOR : Parcelable.Creator<MyEventDay> {
        override fun createFromParcel(parcel: Parcel): MyEventDay {
            return MyEventDay(parcel)
        }

        override fun newArray(size: Int): Array<MyEventDay?> {
            return arrayOfNulls(size)
        }
    }
}
