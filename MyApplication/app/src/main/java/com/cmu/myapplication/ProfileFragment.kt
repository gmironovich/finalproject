import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.cmu.myapplication.MainActivity
import com.cmu.myapplication.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*


class ProfileFragment : Fragment() {

    private lateinit var database : DatabaseReference
    private lateinit var auth : FirebaseAuth

    var firstName : String? = ""
    var lastName : String? = ""
    var occupation : String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_profile, container, false)

        database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mikhgrigplanner.firebaseio.com/").child("users")
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser!!

        database.child(currentUser.uid!!).child("firstName").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                firstName = dataSnapshot.getValue(String::class.java)
                database.child(currentUser.uid!!).child("lastName").addValueEventListener(object :
                    ValueEventListener {
                    override fun onDataChange(dataSnapshot2: DataSnapshot) {
                        lastName = dataSnapshot2.getValue(String::class.java)
                        firstLastName.text = "$firstName" + " $lastName"
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        print("Failed to read value.")
                    }
                })
            }

            override fun onCancelled(databaseError: DatabaseError) {
                print("Failed to read value.")
            }
        })

        database.child(currentUser.uid!!).child("occupation").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                occupation = dataSnapshot.getValue(String::class.java)
                userOccupation.text = occupation
            }

            override fun onCancelled(databaseError: DatabaseError) {
                print("Failed to read value.")
            }
        })

        /*view.onChangePassword.setOnClickListener {view ->
            val newPassword = view.findViewById<EditText>(R.id.newPassword)
            val verifyPassword = view.findViewById<EditText>(R.id.newPassword)

            currentUser?.updatePassword(verifyPassword.text.toString())?.addOnCompleteListener {task ->
                if(task.isSuccessful) {
                    Toast.makeText(activity, "Password was changed!.", Toast.LENGTH_SHORT).show()
                }
            }
        }*/

        view.onSignOut.setOnClickListener {view ->
            auth.signOut()

            val intent = Intent (getActivity(), MainActivity::class.java)
            getActivity()?.startActivity(intent)
        }

        return view
    }

    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment()
    }
}