package com.cmu.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegistrationActivity : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }

    fun onSignUp(v : View) {
        val userEmail = findViewById<EditText>(R.id.registrationUserEmail)
        val userPassword = findViewById<EditText>(R.id.registrationUserPassword)
        val verifyUserPassword = findViewById<EditText>(R.id.registrationVerifyUserPassword)

        if(userPassword.text.toString().equals(verifyUserPassword.text.toString())) {
            auth.createUserWithEmailAndPassword(userEmail.text.toString(), verifyUserPassword.text.toString()).addOnCompleteListener(this) {task ->
                if(task.isSuccessful) {
                    val intent = Intent(this, UserInformationActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(baseContext, "Authentication failed. Passwords do not match!", Toast.LENGTH_SHORT).show()
        }
    }

    fun onSignIn(v : View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}