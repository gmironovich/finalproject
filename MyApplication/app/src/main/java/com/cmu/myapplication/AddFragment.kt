
import android.app.Activity
import android.content.Intent
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.applandeo.materialcalendarview.CalendarView
import androidx.appcompat.app.ActionBar
import com.cmu.myapplication.*
import com.cmu.myapplication.MyEventDay
import com.cmu.myapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_add.view.*

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import java.text.SimpleDateFormat
import java.time.Month
import java.util.*


class AddFragment : Fragment() {

    private lateinit var database : DatabaseReference
    private lateinit var auth : FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mikhgrigplanner.firebaseio.com/")
        auth = FirebaseAuth.getInstance()

        val view: View = inflater.inflate(R.layout.fragment_add, container, false)



        val datePicker = view.findViewById<DatePicker>(R.id.datePicker)
        val noteEditText = view.findViewById<EditText>(R.id.noteEditText)


        view.addNoteButton.setOnClickListener {
            val currentUser = auth.currentUser!!

            val day = datePicker.dayOfMonth
            val month = datePicker.month
            val year = datePicker.year

            val checkDate = Calendar.getInstance()
            checkDate.set(year, month, day)

            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val formatedDate = sdf.format(checkDate.time)

            //val dateCheck = HomeFragment.formatedDate

            val myEventDay = MyEventDay(currentUser.uid,
                formatedDate,
                R.drawable.ic_message_black_48dp, noteEditText.text.toString()
            )

            val event = UUID.randomUUID().toString()

            database.child("events").child(event).setValue(myEventDay)

            val intent = Intent (getActivity(), NavigationActivity::class.java)
            getActivity()?.startActivity(intent)
        }


        return view
    }



    companion object {
        fun newInstance(): AddFragment = AddFragment()
    }
}